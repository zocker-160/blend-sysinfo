# blend-sysinfo

Script which outputs system information using Blender Python API

### How to run

`blender -b --python checker.py`

- (optional) add `-v` to print the data into STDOUT
