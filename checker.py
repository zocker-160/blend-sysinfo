#! /usr/bin/env python3

from io import StringIO
import subprocess
import csv
import shutil
import platform
import re
import bpy
import sys
import cycles
import gpu
import json

def getCPUCuda():
    bpy.context.preferences.addons['cycles'].preferences.refresh_devices()

    devices = bpy.context.preferences.addons['cycles'].preferences.devices

    cpu = [ x["name"] for x in devices if x["type"] == 0 ]
    cuda = [ x["name"] for x in devices if x["type"] != 0 ]

    return cpu, cuda

def parseCudaCap(data: str):
    cudacap = dict()
    currDev = ""

    for entry in data:
        if re.match(r"^[\s]+NVIDIA[\w\d ]+$", entry):
            currDev = entry.strip()
            cudacap[currDev] = dict()

        if re.match(r"^[\s]+CU_DEVICE[\w\d]+[\s]+\d+$", entry):
            if "CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR" in entry \
                or "CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR" in entry:
                key, value, *rest = entry.strip().split()
                cudacap[currDev][key.strip()] = value.strip()

    return cudacap

def cyclesParser(data: str):
    result = dict()
    devices = list()
    cudacap = dict()

    rawdata = data.splitlines(False)

    for i, line in enumerate(rawdata):

        if "CPU device capabilities" in line:
            result["cpuflags"] = line.split(":")[-1].split()

        if re.match(r"^[\s]+NVIDIA[\w\d ]+$", line):
            devices.append(line.strip())

        if "CUDA device capabilities" in line:
            cudacap = parseCudaCap(rawdata[i+1:])

    return result, devices, cudacap

def getGPUDriverVersion():
    if platform.system() == "Linux" and shutil.which("nvidia-smi"):
        # only Linux systems are running headlessly really
        # becauses only real pros don't need a GUI :P
        try:
            vData = subprocess.check_output(
                "nvidia-smi --query-gpu=driver_version --format=csv".split()
                ).decode()

            reader = csv.reader(StringIO(vData))
            for line in reader:
                if re.match(r"^[0-9]{1,3}([.][0-9]{1,2}){1,2}$", line[0]):
                    return line[0]

        except subprocess.CalledProcessError:
            return _getDriverVersionAlt()

    else:
        return _getDriverVersionAlt()

def _getDriverVersionAlt():
    verStr = gpu.platform.version_get().split(' ')
    if "AMD" in gpu.platform.renderer_get() or "Mesa" in verStr:
        return verStr[-2] + ' ' + verStr[-1]
    else:
        return verStr[-1]

def main():
    sysinfo = dict()

    # workaround for segfault on headless systems
    bpy.ops.render.render()

    sysinfo["Blender"] = {
        "platform": bpy.app.build_platform.decode(),
        "version": str(bpy.app.version_string),
        "build date": bpy.app.build_date.decode(),
        "binary path": str(bpy.app.binary_path)
    }


    sysinfo["Python"] = {
        "version": sys.version.split()[0],
        "binary path": sys.executable,
    }

    _cpu, _cuda = getCPUCuda()
    _cycles, _dev, _ccap = cyclesParser(cycles.engine.system_info())

    _plat = {
        "Platform Vendor": gpu.platform.vendor_get(),
        "Platform Version": gpu.platform.version_get(),
        "Platform Renderer": gpu.platform.renderer_get(),
        "Platform Extensions": gpu.capabilities.extensions_get()
    }

    #print(cycles.engine.system_info())
    #print(_cpu)
    #print(_cycles)
    #print(_dev)

    sysinfo["Cycles"] = {
        "cpu": _cpu[0], # there can only be one type of CPU in a system really
        "cpuflags": _cycles.get("cpuflags"),
        "platform": _plat,
        "devices": _dev,
    }

    #sysinfo["cuda"] = _cuda
    sysinfo["cuda attributes"] = _ccap
    sysinfo["GPU driver version"] = getGPUDriverVersion()

    return sysinfo

def printFlags(data: dict):
    print("<cpuflags>")
    for flag in data["Cycles"].get("cpuflags"):
        print(flag)
    print("</cpuflags>")

if __name__ == "__main__":
    info = main()

    bPrint = True if "-v" in sys.argv else False
    bFile = True if "-f" in sys.argv else False
    bFlags = True if "--flags" in sys.argv else False

    if bFile:
        with open("output.json", "w") as f:
            json.dump(info, f, indent=4)

    print("### script output")
    if bPrint:
        print("<jsoninfo>")
        print(info)
        print("</jsoninfo>")
    if bFlags:
        printFlags(info)
    print("###")
